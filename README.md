# Automatic dependency updates using renovate

This repository contains a pipeline schedule to check for dependency updates and
create merge requests for them. This way the projects will be kept up to date
without to many manual interaction.

## Execution steps

The pipeline diverges from the default renovate execution. Reasoning is, that using
GitLab's parent/child relation between pipeline, we can visualize the result of
renovate for each project and potentially rerun the part of an execution that failed.
The downside is a longer execution time of the pipeline.

The execution therefore is split into two sections:

### discover

This step will run renovate to discover repositories configured in the
`RENOVATE_AUTODISCOVER_FILTER`. The result is written to `renovate-repos.json` and
a *downstream* GitLab pipeline is created in `renovate-pipeline.yml`.

### renovate

This step will take `renovate-pipeline.yml` and execute a single downstream pipeline
for each repository discovered during the `discover` step.