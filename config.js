module.exports = {
    "prHourlyLimit": 0,
    "packageRules": [
    {
      "matchUpdateTypes": ["minor", "patch", "pin", "digest"],
      "automerge": true
    }
  ]
};
